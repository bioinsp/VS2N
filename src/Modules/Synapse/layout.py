""" Create a dash layout for the module
"""

from collections import deque
from dash import dcc, html
import traceback
import dash_bootstrap_components as dbc
from .callbacks import callbacks
from src.templates.layoutOp import layoutOp

class layout(layoutOp):
    """ Layout class
    """
    # Needed variables for the graphs --------------------------------
    xAxisSynapseFreqGraph = dict()
    xAxisLabel = dict()
    yAxisSynapseFreqGraph = dict()
    HeatMapSynapseFreqGraph = dict()
    heatmaps = dict()
    globalHeatMap = dict()

    def clearData(self, indexes):
        """ Clear the data when moved forward or backward for more than one step

        indexes (List) : Existing neurons that are displayed
        """
        self.xAxisSynapseFreqGraph.clear()
        self.HeatMapSynapseFreqGraph.clear()
        self.xAxisLabel.clear()
        self.yAxisSynapseFreqGraph.clear()
        self.heatmaps.clear()

        for index in indexes:
            self.xAxisSynapseFreqGraph[index] = deque(maxlen=100)
            self.HeatMapSynapseFreqGraph[index] = deque(maxlen=100)
            self.xAxisLabel[index] = deque(maxlen=100)
            self.yAxisSynapseFreqGraph[index] = deque(maxlen=100)
        
        self.globalHeatMap.clear()

    def Vis(self):
        """ Create layer components

        Args:
            app : Flask app
            g (Global_Var): reference to access global variables

        Returns:
            Dash app layer
        """
        try:
            self.clearData([])
            print("synapse-vis")
            layer = dbc.Card(
                dbc.CardBody(
                    [
                        html.Div(id="synapse-vis", children=[
                        html.Div([
                        dcc.Store(id="heatmapEditInfo",data=[False,False,False]),
                        dcc.Dropdown(
                        id='LayerFilterSynapse',
                        options=[{'label': str(i["layer"]), 'value': str(i["layer"])} for i in (
                        i for i in self.g.LayersNeuronsInfo)], multi=False,
                        style={'width': '150px', "marginLeft": "10px", "textAlign": "start"}),
                        dcc.Dropdown(
                        id='NeuronFilterSynapse', options=[], multi=False,
                        style={'width': '150px', "marginLeft": "10px", "textAlign": "start"}),
                        dbc.Button(html.I(className="fa-solid fa-plus"), id="AddComponentSynapse", n_clicks=0, style={
                        "fontWeight": "500", "marginLeft": "20px", "height": "36px",
                        "backgroundColor":"rgb(68, 71, 99)","borderColor":"rgb(68, 71, 99)"}),
                        html.P("Heatmap Editor:", style={"textAlign": "start", "marginLeft": "20px", "marginTop": "4px"}),
                        dbc.Button(html.I(className="fa-solid fa-left-right"), id="FlipHeatmapH", n_clicks=0, style={"fontWeight": "500", "marginLeft": "20px", "height": "36px","backgroundColor":"rgb(68, 71, 99)","borderColor":"rgb(68, 71, 99)", "color":"white"}),
                        dbc.Button(html.I(className="fa-solid fa-up-down"), id="FlipHeatmapV", n_clicks=0, style={"fontWeight": "500", "marginLeft": "20px", "height": "36px","backgroundColor":"rgb(68, 71, 99)","borderColor":"rgb(68, 71, 99)", "color":"white"}),
                        dbc.Button(html.I(className="fa-solid fa-rotate"), id="RotateHeatmap", n_clicks=0, style={"fontWeight": "500", "marginLeft": "20px", "height": "36px","backgroundColor":"rgb(68, 71, 99)","borderColor":"rgb(68, 71, 99)", "color":"white"}),
                        html.Div(id='clear-Synapse',children="False", style={'display': 'none'}),
                        html.Div(id='display-Synapse',children="False", style={'display': 'none'})
                    ], className="d-flex", style={"paddingBottom": "10px"}),
                        dbc.Card( dbc.CardBody([dbc.Card([dbc.CardHeader(
                        dbc.Button( "Layer HeatMap", color="none",
                        id="group-GlobalHeatMapAreaSynapse-toggle",
                        style={"width":"100%","height":"100%","borderTop":"2px solid rgb(68, 71, 99)","padding":"10px"}
                        ),style={"padding":"0px",}),
                        dbc.Collapse( dbc.CardBody([
                        html.Div(id={'type': "GlobalHeatMapAreaSynapse"}, children=[], 
                        style={"textAlign": "-webkit-center"})]),
                        id="collapse-GlobalHeatMapAreaSynapse")])])),
                        html.Div(id={'type': "GraphsAreaSynapse"}, children=[], 
                        style={"textAlign": "-webkit-center", "paddingTop": "10px"})]),
                    ], style={"textAlign": "center", "padding": "10px"}
                )
            )
            # load callbacks
            callbacks(self,self.app,self.g)
            # Return the Layer
            return layer
        except Exception:
            print("SynapseLayer: " + traceback.format_exc())