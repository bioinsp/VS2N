""" Spark pre-processing operations.
"""

import traceback
from src.templates.sparkOp import sparkOp

def init(g):
    spark(g)
    
class spark(sparkOp):

    def __init__(self, g):
        """ 
        Args:
            g (Global_Var): reference to access global variables
        """
        module = "Neuron"
        document = "spikes"
        output_document = "SpikePerNeuron"
        super().__init__(g, module, document, output_document)

    # Spark operations and preprocessing----------------------------------

    def preProcessing(self):
        """ Apache Spark pre-processing.
        """
        try:
            if (not (self.OUTPUT_DOCUMENT_NAME in self.g.db.list_collection_names())) and (self.DOCUMENT_NAME in self.g.db.list_collection_names()):

                # Spark setup --------------------------------------------
                if self.g.sparkSession == None:
                    self.g.createSparkSession()
                # --------------------------------------------------------
                # Spike per neuron
                df = self.g.sparkSession.read.format("com.mongodb.spark.sql") \
                    .option("spark.mongodb.input.uri", self.MONGODBURL + self.g.name + "."+self.DOCUMENT_NAME+"?authSource=admin&readPreference=primaryPreferred") \
                    .option("pipeline", "[{ $sort : { T : 1} }]")
                
                df = df.load()

                # doing the heavy work -----------------------------------

                df = df.groupBy(df["i"]).count()

                # Data save into MongoDB ---------------------------------

                df.write.format("com.mongodb.spark.sql.DefaultSource") \
                        .option("spark.mongodb.output.uri",
                                self.MONGODBURL + self.g.name + "."+self.OUTPUT_DOCUMENT_NAME+"?authSource=admin&readPreference=primaryPreferred").mode("overwrite").save()

                # --------------------------------------------------------
            else:
                if(not self.DOCUMENT_NAME in self.g.db.list_collection_names()):
                    print(self.DOCUMENT_NAME, "not found")
                    self.g.modules = [module for module in self.g.modules if module != self.MODULE_NAME]
            if self.g.config.DEBUG:
                print("done ", self.MODULE_NAME)

        except Exception:
            print("Error:" + traceback.format_exc())
            pass
