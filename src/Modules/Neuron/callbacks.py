""" This class contains Dash callbacks

    Dash callbacks are the responsible on updating graphs each step.
"""

import dash
import pymongo
import statistics
import traceback
import dash_daq as daq
from collections import deque
import plotly.graph_objects as go
from dash import dcc, html
from bson.json_util import dumps, loads
from dash.dependencies import ALL, MATCH, Input, Output, State
from src.templates.callbacksOp import callbacksOp
from dash import no_update
class callbacks(callbacksOp):
    """ Callbacks class
    """

    def __init__(self, super, app, g):
        """ Initialize the callback .

        Args:
            app : Flask app
            g (Global_Var): reference to access global variables
        """
        # ------------------------------------------------------------
        #     Callbacks
        # ------------------------------------------------------------
        # to prevent creating duplicate callbacks next time
        if not g.checkExistance(app, "NeuronFilterNeuron"):

            try:
                # Callback to handle insert and remove of elements from screen
                @app.callback(Output({"type": "GraphsAreaNeuron"}, "children"), [Input("AddComponentNeuron", "n_clicks"),
                            Input({"type": "DeleteComponent", "index": ALL}, "n_clicks")],
                            [State({"type": "GraphsAreaNeuron"}, "children"), State("LayerFilterNeuron", "value"), State("NeuronFilterNeuron", "value")])
                def InsertRemoveNewElement(addButtonClick, deleteButtonsClick, graphsArea, selectedLayer, selectedNeuron):
                    """ Insert of remove element.

                    Args:
                        addButtonClick (int): number of clicks on the button 
                        deleteButtonsClick (list): number of clicks on all delete buttons 
                        graphsArea (list): list contains all neurons graphs 
                        selectedLayer (String): name of the selected layer
                        selectedNeuron (int): id of selected neuron

                    Returns:
                        the updated content of 'GraphsAreaNeuron'
                    """
                    context = dash.callback_context.triggered

                    if "AddComponentNeuron" in context[0]['prop_id']:
                        # Add item
                        if(selectedLayer != None and selectedNeuron != None):
                            if len(selectedNeuron) > 0:
                                if len([item for item in graphsArea if item["props"]["id"] == "spaceHolder"]) == 1:
                                    updatedBody = [item for item in graphsArea if item["props"]["id"] != "spaceHolder"]
                                                                        
                                    updatedBody.append(addNeuronArea(addButtonClick, selectedLayer, selectedNeuron, g))
                                    return updatedBody
                                else:
                                    graphsArea.append(addNeuronArea(addButtonClick, selectedLayer, selectedNeuron, g))
                                    return graphsArea
                            else:
                                return graphsArea
                        else:
                            return graphsArea
                    else:
                        if(context[0]['prop_id'] != "."):
                            # Delete item
                            itemDeleted = loads(str(context[0]['prop_id']).split(".")[0])["index"]

                            super.xAxisSpikeNbrGraph = ({item: super.xAxisSpikeNbrGraph.get(str(item)) for
                                                         item in super.xAxisSpikeNbrGraph if item != str(itemDeleted)})

                            super.xAxisSpikeNbrLabel = ({item: super.xAxisSpikeNbrLabel.get(str(item)) for
                                                         item in super.xAxisSpikeNbrLabel if item != str(itemDeleted)})
                            
                            super.xAxisPotentialLabel = ({item: super.xAxisPotentialLabel.get(str(item)) for
                                                         item in super.xAxisPotentialLabel if item != str(itemDeleted)})

                            super.xAxisPotentialGraph = ({item: super.xAxisPotentialGraph.get(str(item)) for
                                                         item in super.xAxisPotentialGraph if item != str(itemDeleted)})

                            super.yAxisSpikeNbrGraph = ({item: super.yAxisSpikeNbrGraph.get(str(item)) for
                                                         item in super.yAxisSpikeNbrGraph if item != str(itemDeleted)})

                            super.yAxisPotentialGraph = ({item: super.yAxisPotentialGraph.get(str(item)) for
                                                          item in super.yAxisPotentialGraph if item != str(itemDeleted)})

                            newGraphsArea = [item for item in graphsArea if item["props"]
                                       ["id"] != "VisComponent"+str(itemDeleted)]

                            if(len(newGraphsArea) == 0):
                                newGraphsArea.append(addSpaceHolder(app))
                                
                            return newGraphsArea
                        else:
                            if(len(graphsArea) == 0):
                                graphsArea.append(addSpaceHolder(app))
                            return graphsArea
            except Exception:
                print("InsertRemoveNewElement: " + traceback.format_exc())


            try:
                # Callback to get neurons of seleced layer
                @app.callback([Output("NeuronFilterNeuron", "options"), Output("NeuronFilterNeuron", "value")], [Input("LayerFilterNeuron", "value")])
                def NeuronSelection(selectedLayer):
                    """ Get neurons of the selected layer.

                    Args:
                        selectedLayer (String): selected layer name

                    Returns:
                        list of neurons for neurons menu component
                    """
                    if selectedLayer != None:
                        return [[{'label': str(i), 'value': str(i)} for i in range(g.Layer_Neuron[selectedLayer])], ""]
                    else:
                        return [[], ""]
            except Exception:
                print("NeuronSelection: " + traceback.format_exc())

            try:
                # Callback to handle filtering when 3D view clicked
                @app.callback([Output("SpikePerNeuronNbr", "figure"), Output("SpikePerNeuronNbr", "style")], [Input("SpikePerNeuronFreq", "clickData")])
                def NeuronFreqFilter(filterClass):
                    """ Generate a graph of neurons spikes activity from selected filterClass.

                    Args:
                        filterClass (String): filter selected

                    Returns:
                        a graph that contains number of spikes per neuron for the selected class
                    """
                    if filterClass == None:
                        return [super.SpikesSameClass(filterClass, g), {"visibility": "hidden"}]
                    else:
                        return [super.SpikesSameClass(filterClass["points"][0], g), {"visibility": "visible"}]
            except Exception:
                print("NeuronFreqFilter: " + traceback.format_exc())

            try:
                # Callback to handle clearing content when received a clear signal
                @app.callback([Output("display-Neuron", "children"), Output("clear-Neuron", "children")], [Input("v-step", "children")], [State("interval", "value"), State("clear", "children"), State("clear-Neuron", "children"), State("display-Neuron", "children"),State({"type": "GraphsAreaNeuron"}, "children")])
                def clearProcess(sliderValue, updateInterval, clearGraphs, localClear, displayContent, graphContent):
                    """ Clear the display if it is required.

                    Args:
                        sliderValue (int): value of the slider
                        updateInterval (int): update Interval (s)
                        clearGraphs (boolean): a dash state to pass information to all visualization about clearing content (if needed)
                        localClear (boolean): local value of the global clear variable stored for comparaison purposes
                        displayContent (boolean): when this value change all graphs will be updated
                        graphContent : displayed neurons information

                    Returns:
                        boolean array
                    """
                    if(localClear != clearGraphs):
                        existingIndexes = [item["props"]["id"].split("VisComponent")[1] for item in graphContent if "VisComponent" in item["props"]["id"]]
                        super.clearData(existingIndexes)

                    return [displayContent, clearGraphs]
            except Exception:
                print("clearProcess: "+ traceback.format_exc())
            try:
                # Callback to handle spikes per interval graph and spike class representation
                @app.callback([Output({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": "FreqGraph"}, "figure"),
                               Output({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": "AccuracyGraph"}, "figure")], [Input("display-Neuron", "children")],
                              [State("v-step", "children"), State("interval", "value"), State({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": "FreqGraph"}, "id"), State({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": 'Spikes-number-switch'}, 'on'), State({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": 'Neuron-class-switch'}, 'on')])
                def processSpikeRelatedGraphs(displayContent, sliderValue, updateInterval, selectedItem, isOnSpike, isOnAcc):
                    """ Calculate number of spikes per step for one neuron

                    Args:
                        displayContent (boolean): when this value change all graphs will be updated
                        sliderValue (int): value of the slider
                        updateInterval (int): update Interval (s)
                        selectedItem (list): selected neuron
                        isOnSpike (bool): whether spikes number graph is active or not
                        isOnAcc (bool): whether spikes class graph is active or not

                    Raises:
                        no_update: in case we don't want to update the content we rise this execption

                    Returns:
                        three graphs of the selected neuron
                    """
                    if dash.callback_context.triggered[0]["prop_id"] != '.':
                        # data processing
                        if(sliderValue > 0 and sliderValue <= g.stepMax):
                            data = getSpikesOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                            if (len(super.xAxisSpikeNbrLabel[selectedItem["index"]]) == 0 or super.xAxisSpikeNbrLabel[selectedItem["index"]][-1] != ("["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")):

                                if len(super.xAxisSpikeNbrGraph[selectedItem["index"]]) > 0:
                                    super.xAxisSpikeNbrGraph[selectedItem["index"]].append(
                                        super.xAxisSpikeNbrGraph[selectedItem["index"]][-1]+1)
                                    super.xAxisSpikeNbrLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")
                                else:
                                    super.xAxisSpikeNbrGraph[selectedItem["index"]].append(1)
                                    super.xAxisSpikeNbrLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                                output = [spikeCountDrawGraph(selectedItem["index"], data, super.xAxisSpikeNbrGraph[selectedItem["index"]], super.xAxisSpikeNbrLabel[selectedItem["index"]], super.yAxisSpikeNbrGraph, isOnSpike),
                                        classDrawGraph(selectedItem["index"], data, isOnAcc, g.finalLabels)]

                                return output
                            else:
                                return no_update
                        else:
                            if(sliderValue == 0):
                                data = getSpikesOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                                super.xAxisSpikeNbrGraph[selectedItem["index"]].append(sliderValue)

                                super.xAxisSpikeNbrLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                                output = [spikeCountDrawGraph(selectedItem["index"], data, super.xAxisSpikeNbrGraph[selectedItem["index"]], super.xAxisSpikeNbrLabel[selectedItem["index"]], super.yAxisSpikeNbrGraph, isOnSpike),
                                        classDrawGraph(selectedItem["index"], data, isOnAcc, g.finalLabels)]
                                
                                return output
                            else:
                                return no_update
                    else:
                        # after adding to the screen
                        if(selectedItem["index"] not in super.xAxisSpikeNbrLabel):
                            data = getSpikesOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                            super.xAxisSpikeNbrGraph[selectedItem["index"]] = deque(maxlen=100)
                            super.xAxisSpikeNbrLabel[selectedItem["index"]] = deque(maxlen=100)

                            super.xAxisSpikeNbrGraph[selectedItem["index"]].append(sliderValue)

                            super.xAxisSpikeNbrLabel[selectedItem["index"]].append("["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                            output = [spikeCountDrawGraph(selectedItem["index"], data, super.xAxisSpikeNbrGraph[selectedItem["index"]], super.xAxisSpikeNbrLabel[selectedItem["index"]], super.yAxisSpikeNbrGraph, isOnSpike),
                                        classDrawGraph(selectedItem["index"], data, isOnAcc, g.finalLabels)]
                            
                            return output
                        else:
                            return no_update
            except Exception:
                print("processSpikeRelatedGraphs: " + traceback.format_exc())

            try:
                # Callback to handle neuron potential graph
                @app.callback([Output({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": "PotentialGraph"}, "figure")], [Input("display-Neuron", "children")],
                              [State("v-step", "children"), State("interval", "value"), State({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": "PotentialGraph"}, "id"), State({"index": MATCH, "layer": MATCH, "neuron": MATCH, "type": 'Neuron-potential-switch'}, 'on')])
                def processPotential(displayContent, sliderValue, updateInterval, selectedItem, isOn):
                    """Processes potentials and updates the corresponding feature .

                    Args:
                        displayContent (boolean): when this value change all graphs will be updated
                        sliderValue (int): value of the slider
                        updateInterval (int): update Interval (s)
                        selectedItem (list): selected neuron
                        isOn (bool): whether this graph is active or not

                    Raises:
return no_update: in case we don't want to update the content we rise this execption

                    Returns:
                        neuron potential graph of the selected neuron
                    """
                    if dash.callback_context.triggered[0]["prop_id"] != '.':
                        # data processing
                        if(sliderValue > 0 and sliderValue <= g.stepMax):
                            data = getPotentialOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                            if (len(super.xAxisPotentialLabel[selectedItem["index"]]) == 0 or super.xAxisPotentialLabel[selectedItem["index"]][-1] != ("["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")):

                                if len(super.xAxisPotentialGraph[selectedItem["index"]]) > 0:
                                    super.xAxisPotentialGraph[selectedItem["index"]].append(
                                        super.xAxisPotentialGraph[selectedItem["index"]][-1]+1)
                                    super.xAxisPotentialLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")
                                else:
                                    super.xAxisPotentialGraph[selectedItem["index"]].append(1)
                                    super.xAxisPotentialLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                                output = [neuronPotentialDrawGraph(selectedItem["index"], data, super.xAxisPotentialGraph[selectedItem["index"]], super.xAxisPotentialLabel[selectedItem["index"]], super.yAxisPotentialGraph, isOn)]
                                return output
                            else:
                                return no_update
                        else:
                            if(sliderValue == 0):
                                data = getPotentialOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                                super.xAxisPotentialGraph[selectedItem["index"]].append(sliderValue)

                                super.xAxisPotentialLabel[selectedItem["index"]].append(
                                        "["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                                output = [neuronPotentialDrawGraph(selectedItem["index"], data, super.xAxisPotentialGraph[selectedItem["index"]], super.xAxisPotentialLabel[selectedItem["index"]], super.yAxisPotentialGraph, isOn)]
                                
                                return output
                            else:
                                return no_update
                    else:
                        # after adding to the screen
                        if(selectedItem["index"] not in super.xAxisPotentialLabel):
                            data = getPotentialOfNeuron(
                                int(sliderValue)*float(updateInterval), g, selectedItem)

                            super.xAxisPotentialGraph[selectedItem["index"]] = deque(maxlen=100)
                            super.xAxisPotentialLabel[selectedItem["index"]] = deque(maxlen=100)

                            super.xAxisPotentialGraph[selectedItem["index"]].append(sliderValue)

                            super.xAxisPotentialLabel[selectedItem["index"]].append("["+g.getLabelTime(g.updateInterval, sliderValue)+","+g.getLabelTime(g.updateInterval, sliderValue+1)+"]")

                            output = [neuronPotentialDrawGraph(selectedItem["index"], data, super.xAxisPotentialGraph[selectedItem["index"]], super.xAxisPotentialLabel[selectedItem["index"]], super.yAxisPotentialGraph, isOn)]
                            
                            return output
                        else:
                            return no_update
            except Exception:
                print("processPotential: " + traceback.format_exc())

        try:

            # ------------------------------------------------------------
            # Helper functions
            # ------------------------------------------------------------

            def addNeuronArea(index, layer, neuron, g):
                """ Adds a new neuron area that contains graphs.

                Args:
                    index (int): index of the new added area
                    layer (String): layer id
                    neuron (String): neuron id
                    g (Global_Var): reference to access global variables

                Returns:
                    html component that contains the neuron area graphs
                """
                return html.Div([
                    html.Div([
                        html.Div([
                            html.Div([html.P("Spikes", style={"textAlign": "start", "marginLeft": "20px", "marginTop": "6px", "fontSize": "13px"}),
                            daq.PowerButton(
                            id={"index": str(index), "layer": layer, "neuron": neuron,
                            "type": "Spikes-number-switch"},
                            on='True',
                            size=25,
                            color="#28a745",
                            style={"marginLeft": "10px","marginRight": "10px"}
                            ),
                            html.Button(html.I(className="fa-solid fa-trash"), style={"fontWeight": "500","fontSize":"16px", "marginLeft": "10px","color":"red","backgroundColor":"#00110000","border":"0"}, id={"index": str(index), "type": "DeleteComponent"}),html.Div([html.Span(layer+" N: "+neuron, style={"fontSize": "13px", "paddingTop": "10px", "marginRight": "10px"}, className="badge alert-info")], className="d-flex", style={"direction": "rtl", "width": "100%"})], className="d-flex", style={"height": "35px"}),
                            dcc.Graph(id={"index": str(index), "type": "FreqGraph", "layer": layer, "neuron": neuron}, style={
                            "width": "100%", "height": "290px"}, className="col-12", animate=False, config={"displaylogo": False}),
                        ], className="col-lg-4 col-sm-12 col-xs-12" if(g.finalLabels != None) else "col-lg-6 col-sm-12 col-xs-12"),
                        html.Div([
                            html.Div([html.P("Potential", style={"textAlign": "start", "marginLeft": "20px", "marginTop": "6px", "fontSize": "13px"}),
                            daq.PowerButton(
                            id={"index": str(index), "type": "Neuron-potential-switch",
                            "layer": layer, "neuron": neuron},
                            on='True',
                            size=25,
                            color="#28a745",
                            style={"marginLeft": "10px"}
                            )], className="d-flex", style={"height": "35px"}),
                            dcc.Graph(id={"index": str(index), "type": "PotentialGraph", "layer": layer, "neuron": neuron}, style={
                            "width": "100%", "height": "290px"}, className="col-12", animate=False, config={"displaylogo": False}),
                        ], className="col-lg-4 col-sm-6 col-xs-6" if(g.finalLabels != None) else "col-lg-6 col-sm-12 col-xs-12"),
                        html.Div([
                            html.Div([html.P("Class", style={"textAlign": "start", "marginLeft": "20px", "marginTop": "6px", "fontSize": "13px"}),
                            daq.PowerButton(
                            id={"index": str(index), "type": "Neuron-class-switch",
                            "layer": layer, "neuron": neuron},
                            on='True',
                            size=25,
                            color="#28a745",
                            style={"marginLeft": "10px"})], className="row", style={"height": "35px"}),
                            dcc.Graph(id={"index": str(index), "type": "AccuracyGraph", "layer": layer, "neuron": neuron}, style={
                            "width": "100%", "height": "290px"}, className="col-12", animate=False, config={"displaylogo": False})], className="col-lg-4 col-sm-6 col-xs-6" if(g.finalLabels != None) else "", style={} if(g.finalLabels != None) else {'display': 'none'})],className="row")], style={"background": "rgb(242, 248, 255)", "paddingBottom": "10px", "marginBottom": "10px", "padding": "5px", "borderRadius": "10px"}, id="VisComponent"+str(index))

            def addSpaceHolder(app):
                """ Adds a space holder area when no graphs are selected.

                Args:
                    app : Flask app
                Returns:
                    html component that contains the neuron area graphs
                """
                return html.Div([
                    html.Div([html.Img(src=app.get_asset_url('holderImg.png'), className="col-6", style={"marginTop":"60px"})], 
                            className="col-12", style={"width": "100%"})
                ], style={"background": "rgb(244 244 244)", "paddingBottom": "10px", "marginBottom": "10px", "margin": "5px", "borderRadius": "10px", "height":"335px"},
                    id="spaceHolder")


            def spikeCountDrawGraph(index, data, xAxis, xAxisLabel, yAxisList, isOn):
                """ Create bar chart for selected neuron, that contains 
                spikes count per interval.

                Args:
                    index (int): index of the new added area
                    data (list): data to be added to the graph
                    xAxis (deque): X axis values
                    xAxisLabel (deque): X axis labels
                    yAxisList (dict): dict contains Y axis values
                    isOn (bool): whether this graph is active or not

                Returns:
                    bar chart content (data and layout)
                """
                try:
                    if isOn:
                        if data != None:

                            # filtering data based on layer and neuron
                            if str(index) not in yAxisList:
                                yAxisList[str(index)] = deque(maxlen=100)

                            # add data
                            yAxisList[str(index)].append(len(data))

                            return {'data': [
                                go.Bar(
                                x=list(xAxis),
                                y=list(yAxisList[str(index)]),
                                name='',
                                width=0.5,
                                marker_color='rgb(31, 119, 180)',
                                text=list(xAxisLabel),
                                textposition="none",
                                hovertemplate="%{text} <b><br>%{y}</b>"
                                )],
                                'layout': go.Layout(
                                xaxis_type='category',
                                margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                                uirevision='no reset of zoom',
                                yaxis={'title': 'Number of Spikes'},
                                xaxis={'title': 'Step'},
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)"

                            )}
                        else:
                            if str(index) not in yAxisList:
                                yAxisList[str(index)] = deque(maxlen=100)

                            yAxisList[str(index)].append(0)
                            return {'data': [
                                go.Bar(
                                x=list(xAxis),
                                y=list(yAxisList[str(index)]),
                                marker_color='rgb(31, 119, 180)',
                                name='',
                                text=list(xAxisLabel),
                                textposition="none",
                                hovertemplate="%{text} <b><br>%{y}</b>"
                                )],
                                'layout': go.Layout(
                                margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                                uirevision='no reset of zoom',
                                yaxis={'title': 'Number of Spikes'},
                                xaxis={'title': 'Step'},
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)"
                            )}
                    else:
                        return {'data': [
                            go.Bar(
                            x=list(xAxis),
                            y=list(yAxisList[str(index)]),
                            marker_color='rgb(31, 119, 180)',
                            text=list(xAxisLabel),
                            textposition="none",
                            name='',
                            hovertemplate="%{text} <b><br>%{y}</b>"
                            )],
                            'layout': go.Layout(
                            margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                            uirevision='no reset of zoom',
                            yaxis={'title': 'Number of Spikes'},
                            xaxis={'title': 'Step'},
                            paper_bgcolor= "rgba(255, 255, 255,0)",
                            plot_bgcolor= "rgba(255, 255, 255,0)"
                        )}
                except Exception:
                    print("spikeCountDrawGraph: "+ traceback.format_exc())

            def neuronPotentialDrawGraph(index, data, xAxis, xAxisLabel, yAxisList, isOn):
                """ Create scatter plot for selected neuron, that contains 
                membrane potential per interval.

                Args:
                    index (int): index of the new added area
                    data (list): data to be added to the graph
                    xAxis (deque): X axis values
                    xAxisLabel (deque): X axis labels
                    yAxisList (dict): dict contains Y axis values
                    isOn (bool): whether this graph is active or not

                Returns:
                    scatter plot content (data and layout)
                """
                try:

                    if isOn:
                        if data != None:

                            # filtering data based on layer and neuron
                            if str(index) not in yAxisList:
                                yAxisList[str(index)] = deque(maxlen=100)

                            # add data
                            if len(data) != 0:
                                yAxisList[str(index)].append(
                                    statistics.mean([d["V"] for d in data]))

                            return {'data': [
                                go.Scatter(
                                x=list(xAxis),
                                y=list(yAxisList[str(index)]),
                                mode='lines+markers',
                                name='',
                                fill='tozeroy',
                                marker_color='rgb(44, 160, 44)',
                                text=list(xAxisLabel),
                                hovertemplate="%{text} <b><br>%{y}</b>"
                                )],
                                'layout': go.Layout(
                                margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                                uirevision='no reset of zoom',
                                yaxis={'title': 'Potential'},
                                xaxis={'title': 'Step'},
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)"
                            )}
                        else:
                            if str(index) not in yAxisList:
                                yAxisList[str(index)] = deque(maxlen=100)
                                yAxisList[str(index)].append(0)
                            else:
                                if(len(yAxisList[str(index)]) > 0):
                                    yAxisList[str(index)].append(yAxisList[str(index)][-1])                        
                                else:
                                    yAxisList[str(index)].append(0)
                                    
                            return {'data': [
                                go.Scatter(
                                x=list(xAxis),
                                y=list(yAxisList[str(index)]),
                                mode='lines+markers',
                                name='',
                                fill='tozeroy',
                                marker_color='rgb(44, 160, 44)',
                                text=list(xAxisLabel),
                                hovertemplate="%{text} <b><br>%{y}</b>"
                                )],
                                'layout': go.Layout(
                                margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                                uirevision='no reset of zoom',
                                yaxis={'title': 'Potential'},
                                xaxis={'title': 'Step'},
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)"
                            )}
                    else:
                        if str(index) not in yAxisList:
                            yAxisList[str(index)] = deque(maxlen=100)

                        return {'data': [
                            go.Scatter(
                            x=list(xAxis),
                            y=list(yAxisList[str(index)]),
                            mode='lines+markers',
                            name='',
                            fill='tozeroy',
                            marker_color='rgb(44, 160, 44)',
                            text=list(xAxisLabel),
                            hovertemplate="%{text} <b><br>%{y}</b>"
                            )],
                            'layout': go.Layout(
                            margin={'l': 40, 'r': 0, 't': 0, 'b': 35},
                            uirevision='no reset of zoom',
                            yaxis={'title': 'Potential'},
                            xaxis={'title': 'Step'},
                            paper_bgcolor= "rgba(255, 255, 255,0)",
                            plot_bgcolor= "rgba(255, 255, 255,0)"
                        )}
                except Exception:
                    print("neuronPotentialDrawGraph: "+ traceback.format_exc())

            def classDrawGraph(index, data, isOn, labels):
                """ Create scatter polar plot for selected neuron, that represents 
                class responsible on firing theis neuron.

                Args:
                    index (int): index of the new added area
                    data (list): data to be added to the graph
                    isOn (bool): whether this graph is active or not
                    labels (list): list of labels
                Returns:
                    scatter polar plot content (data and layout)
                """
                try:
                    if isOn:
                        if data != None and labels != None:

                            labels = list(dict.fromkeys(
                                [("Class:"+str(l["Label"]), 0) for l in labels]))
                            labels = [list(l) for l in labels]

                            for d in data:
                                for l in labels:
                                    if "Class:"+str(d['Input']) == str(l[0]):
                                        l[1] = l[1]+1

                            theta = [l[0] for l in labels]
                            labels = [l[1] for l in labels]
                            return {'data': [
                                go.Scatterpolar(
                                theta=theta,
                                r=labels,
                                fill='toself',
                                fillcolor='rgba(255, 127, 14)',
                                line=dict(color="rgb(255, 127, 14)")
                                )],
                                'layout': go.Layout(
                                polar=dict(
                                    angularaxis=dict(
                                    direction="clockwise",
                                    period=6),
                                    radialaxis=dict(
                                    visible=False
                                    )
                                ),
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)",
                                showlegend=False,
                                margin={'l': 0, 'r': 0, 't': 25, 'b': 30},
                            )
                            }
                        else:
                            return {'data': [
                                go.Scatterpolar(
                                theta=[],
                                r=[],
                                fill='toself',
                                fillcolor='rgba(255, 127, 14)',
                                line=dict(color="rgb(255, 127, 14)")
                                )],
                                'layout': go.Layout(
                                polar=dict(
                                    radialaxis=dict(
                                    visible=True
                                    )
                                ),
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)",
                                showlegend=False,
                                margin={'l': 0, 'r': 0, 't': 25, 'b': 30},
                            )
                            }
                    else:
                        return {'data': [
                                go.Scatterpolar(
                                theta=[],
                                r=[],
                                fill='toself',
                                fillcolor='rgba(255, 127, 14)',
                                line=dict(color="rgb(255, 127, 14)")
                                )],
                                'layout': go.Layout(
                                polar=dict(
                                    radialaxis=dict(
                                    visible=True
                                    ),
                                paper_bgcolor= "rgba(255, 255, 255,0)",
                                plot_bgcolor= "rgba(255, 255, 255,0)"
                                ),
                            showlegend=False,
                            margin={'l': 0, 'r': 0, 't': 25, 'b': 30},
                        )
                        }
                except Exception:
                    print("classDrawGraph: "+ traceback.format_exc())

            # ------------------------------------------------------------
            # MongoDB operations
            # ------------------------------------------------------------

            def getSpikesOfNeuron(timestamp, g, neuronInfo):
                """ Get spikes per neuron activity in a given interval.

                Args:
                    timestamp (int): timestamp value
                    g (Global_Var): reference to access global variables
                    neuronInfo (list): selected neuron information

                Returns:
                    spikes count
                """
                layer = [neuronInfo["layer"]]
                neuron = [int(neuronInfo["neuron"])]

                # MongoDB---------------------
                col = pymongo.collection.Collection(g.db, 'spikes')
                spikeOfNeuron = col.find({"T": {'$gt': timestamp, '$lte': (
                    timestamp+g.updateInterval)}, "i.L": {'$in': layer}, "i.N": {'$in': neuron}})
                # ----------------------------

                # ToJson----------------------
                spikeOfNeuron = loads(dumps(spikeOfNeuron))
                # ----------------------------

                if not spikeOfNeuron:
                    return None
                return spikeOfNeuron

            def getPotentialOfNeuron(timestamp, g, neuronInfo):
                """ Get neuron membrane potential activity in a given interval.

                Args:
                    timestamp (int): timestamp value
                    g (Global_Var): reference to access global variables
                    neuronInfo (list): selected neuron information

                Returns:
                    neuron membrane potential value
                """
                layer = [neuronInfo["layer"]]
                neuron = [int(neuronInfo["neuron"])]
                # MongoDB---------------------
                col = pymongo.collection.Collection(g.db, 'potential')
                FreqForNeuron = col.find(
                    {"T": {'$gt': timestamp, '$lte': (timestamp+g.updateInterval)}, 
                    "L": {'$in': layer}, "N": {'$in': neuron}})
                # ----------------------------

                # ToJson----------------------
                FreqForNeuron = loads(dumps(FreqForNeuron))
                # ----------------------------

                if not FreqForNeuron:
                    return None
                return FreqForNeuron

        except Exception:
            print("Helper functions: "+ traceback.format_exc())