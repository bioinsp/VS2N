""" Spark pre-processing operations .
"""

import traceback
from src.templates.sparkOp import sparkOp

def init(g):
    spark(g)

class spark(sparkOp):

    def __init__(self, g):
        """
            Args:
            g (Global_Var): reference to access global variables
        """
        module = ""
        document = ""
        output_document = ""
        super().__init__(g, module, document, output_document)

    # Spark operations and pre-processing --------------------------------


    def preProcessing(self):
        """ Spark preprocessing .
        """
        MODULE_NAME = ""
        DOCUMENT_NAME = ""
        OUTPUT_DOCUMENT_NAME = ""
        MONGODBURL = self.g.MONGODBURL
        try:
            if (not (OUTPUT_DOCUMENT_NAME in self.g.db.list_collection_names())) and (DOCUMENT_NAME in self.g.db.list_collection_names()):

                # Spark setup --------------------------------------------

                if self.g.sparkSession == None:
                    self.g.createSparkSession()

                # doing the heavy work -----------------------------------

                # Save data into Mongo------------------------------------

                #df.write.format("com.mongodb.spark.sql.DefaultSource") \
                #        .option("spark.mongodb.output.uri",
                #                MONGODBURL + self.g.name + "."+OUTPUT_DOCUMENT_NAME+"?authSource=admin&#readPreference=primaryPreferred").mode("overwrite").save()

                #print("Saved to BDD")

            # ------------------------------------------------------------
            else:
                if(not DOCUMENT_NAME in self.g.db.list_collection_names()):
                    print(DOCUMENT_NAME, "not found")
                    self.g.modules = [module for module in self.g.modules if module != MODULE_NAME]
            print("done ", MODULE_NAME)

        except Exception:
            print("Error:" + traceback.format_exc())
            pass