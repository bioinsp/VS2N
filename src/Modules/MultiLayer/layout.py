"""Create a dash layout for the module
"""

from collections import deque
from dash import dcc, html
import traceback
import dash_bootstrap_components as dbc
from .callbacks import callbacks
from src.templates.layoutOp import layoutOp

class layout(layoutOp):
    """ Layout class
    """
    # Needed variables for the graphs --------------------------------
    #
    xAxisGraphName = deque(maxlen=100)
    #
    # ----------------------------------------------------------------

    def clearData(self):
        """ Clear the data when moved forward or backward for more than one step
        """
        self.xAxisGraphName.clear()

    def Vis(self):
        """ Create layer components

        Args:
            app : Flask app
            g (Global_Var): reference to access global variables

        Returns:
            Dash app layer
        """
        try:

            layer = dbc.Card(
                dbc.CardBody(
                    [
                        html.Div(id="multilayer-vis", children=[
                            # Global show based on selected layer
                            html.H3("Your Layer here"),
                            html.Div(id='clear-multilayer', children="False", style={'display': 'none'}), 
                            html.Div(id='display-multilayer', children="False", style={'display': 'none'})
                        ])
                    ], style={"textAlign": "center", "padding": "10px"}
                )
            )
            # load callbacks
            # callbacks(self.app, self.g)
            # Return the Layer
            return layer
        except Exception:
            print("MultiLayer:" + traceback.format_exc())