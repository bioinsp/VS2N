""" This class contains Dash callbacks

    Dash callbacks are the responsible on updating graphs each step.
"""
import dash
import pymongo
import statistics
import dash_daq as daq
from collections import deque
import plotly.graph_objects as go
from dash import dcc, html
from bson.json_util import dumps, loads
from dash.exceptions import PreventUpdate
import traceback
from dash.dependencies import ALL, MATCH, Input, Output, State
from src.templates.callbacksOp import callbacksOp

class callbacks(callbacksOp):
    """Callbacks class
    """

    def __init__(self, super, app, g):
        """Initialize the callback .

        Args:
            app : Flask app
            g (Global_Var): reference to access global variables
        """
        # ------------------------------------------------------------
        #     Callbacks
        # ------------------------------------------------------------
        # to prevent creating duplicate callbacks next time
        if not g.checkExistance(app, "name of one element from layout"):

            # Add callbacks here
            # Use Input("v-step", "children"),State("interval",
            # "value") to keep track of time slider
            try:
                # @app.callback([Output()], [Input()])
                # def NeuronSelection(input):
                # do process here
                #    return output
                print("Add Callbacks here")

                try:
                    @app.callback([Output("display-multilayer", "children"), Output("clear-multilayer", "children")],
                                  [Input("v-step", "children")], [State("interval", "value"), State("clear", "children"),State("clear-Neuron", "children"), State("display-Neuron", "children")])
                    def display(value, step, clear, localClear, display):
                        if(localClear != clear):
                            super.clearData()

                        return [display, clear]
                except Exception:
                    print("display:" + traceback.format_exc())

            except Exception:
                print("CallbackName:" + traceback.format_exc())

            # ----------------------------------------------------------------
            # Callback related Data retreiving functions
            # ----------------------------------------------------------------
            try:

                print("Add functions here")
                # ------------------------------------------------------------
                # Data gethering (MongoDB operations)
                # ------------------------------------------------------------

                print("Add MongoDB functions here")

            except Exception:
                print("Data process loading:" + traceback.format_exc())