""" Spark pre-processing operations.
"""

from bson.json_util import dumps
from bson.json_util import loads
import traceback
import pymongo
from src.templates.sparkOp import sparkOp

def init(g):
    spark(g)
    
class spark(sparkOp):
    def __init__(self, g):
        module = "General"
        document = ""
        output_document = ""
        super().__init__(g, module, document, output_document)

    # Spark operations and pre-processing --------------------------------

    def preProcessing(self):
        """ Apache Spark pre-processing.
        """
        try:
            # initial variables
            self.g.LayersNeuronsInfo = []
            self.g.NeuronsNbr = 0
            self.g.LayersNbr = 0
            # get info collection
            col = pymongo.collection.Collection(self.g.db, 'info').find()
            data = next(col, None)
            # get dataset name
            self.g.Dataset = data["D"]
            # get network layers and neurons
            LN = data["L:N"]
            LN = loads(dumps(LN))
            self.g.Layer_Neuron = LN
            for l in LN:
                if(l == "Input"):
                    self.g.Input = LN[l]
                elif("pool" not in l):
                    self.g.LayersNeuronsInfo.append(
                        {"layer": l, "neuronNbr": int(LN[l])})
                    self.g.NeuronsNbr += int(LN[l])
                    self.g.LayersNbr += 1

            #pymongo.collection.Collection(self.g.db, 'synapseWeight').find_one(sort=[("index.x", -1),("index.y", -1)])["index"]

            # get date & time of the simulation
            self.g.Date = data["T"]
            # calculate simulation time

            try:
                M = 0
                if ('spikes' in self.g.db.list_collection_names()):
                    M = max(M, pymongo.collection.Collection(
                        self.g.db, 'spikes').find_one(sort=[("T", -1)])["T"])
                if ('potential' in self.g.db.list_collection_names()):
                    M = max(M, pymongo.collection.Collection(
                        self.g.db, 'potential').find_one(sort=[("T", -1)])["T"])
                if ('synapseWeight' in self.g.db.list_collection_names()):
                    M = max(M, pymongo.collection.Collection(
                        self.g.db, 'synapseWeight').find_one(sort=[("T", -1)])["T"])
                if ('labels' in self.g.db.list_collection_names()):
                    self.g.labelsExistance = True
                    M = max(M, pymongo.collection.Collection(self.g.db, 'labels').find_one(sort=[("T", -1)])["T"])
                    self.g.ClassNbr = int(pymongo.collection.Collection(self.g.db, 'labels').find_one(sort=[("L", -1)])["L"])+1
                else:
                    print("No labels")
            
            except Exception:
                print("GlobalSparkMongoError:" + traceback.format_exc())
                pass
            self.g.Max = M
            self.g.stepMax = int(M/self.g.updateInterval)+1
            # get accuracy and neurons labels (if exist)
            try:
                data = next(col, None)
                self.g.Accuracy = data["Acc"]
            except Exception:
                self.g.Accuracy = "--"
                print("No accuracy recorded")
                pass
            
            try:
                self.g.finalLabels = data["NLabel"]
            except Exception:
                print("No output neuron classes recorded")
                self.g.finalLabels = None
                pass

            # ------------------------------------------------------------
            if self.g.config.DEBUG:
                print("done ", self.MODULE_NAME)

        except Exception:
            print("GlobalSpark:" + traceback.format_exc())
            pass
