""" Dash layout template .
"""

class layoutOp():
    """ Layout class
    """

    def load(self, app, g):
        """ 
        Args:
            app : Flask app
            g (Global_Var): reference to access global variables
        """
        self.app = app 
        self.g = g
        return self.Vis()

    def clearData(self):
        """ Clear the data when moved forward or backward for more than one step
        """
        print("Clear different deques")

    def Vis(self):
        """ Create layer components

        Returns:
            Dash app layer
        """
        print("Layout here")