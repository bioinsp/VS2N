""" Spark pre-processing template .
"""

class sparkOp():
    def __init__(self,g,module,document,output_document):
        """ 
        Args:
            g (Global_Var): reference to access global variables
        """
        self.g = g
        self.MODULE_NAME = module
        self.DOCUMENT_NAME = document
        self.OUTPUT_DOCUMENT_NAME = output_document
        self.MONGODBURL = g.MONGODBURL
        self.preProcessing()
        self.g.modulesNbr = self.g.modulesNbr - 1

    # Spark operations and pre-processing --------------------------------

    def preProcessing(self):
        """ Preprocessing using Spark and storing data again in DB.
        """
        print("Add spark operations here")