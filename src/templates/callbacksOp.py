""" Dash callbacks template. 
"""


from dash.dependencies import Output, Input, State


class callbacksOp():

    def __init__(self, app, g):
        self.app = app 
        self.g = g

        """Initialize the callback .

        Args:
            app : Flask app
            g (Global_Var): reference to access global variables
        """
        # ------------------------------------------------------------
        #     Callbacks
        # ------------------------------------------------------------
        # to prevent creating duplicate callbacks next time (to be copied)
        if not g.checkExistance(app, "name of one element from layout"):

            try:
                @app.callback([Output("display-template", "children"), Output("clear-template", "children")],
                                [Input("v-step", "children")], [State("interval", "value"), State("clear", "children"),
                                                                State("clear-Neuron", "children"), State("display-Neuron", "children")])
                def display(value, step, clear, localClear, display):
                    if(localClear != clear):
                        super.clearData()

                    return [display, clear]
            except Exception as e:
                print("display:"+str(e))