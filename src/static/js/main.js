$(document).ready(function () {

    var selectedButton, i;
    var modules = [];
    $('button').on('click', function (event) {

        i = $(this)["0"].id;
        selectedButton = $($(event.target));
        if (selectedButton[0].data == null) {
            $($('#' + i)).prop("data", "false");
        }
        if (selectedButton[0].data == "false") {
            console.log("selected");
            $($('#' + i)).prop("data", "true");
            $($('#' + i)).css({ "background-color": "#28a745", "color": "white" });
            modules.push($($('#' + i)).attr("data-value"));
        } else {
            console.log("not selected");
            $($('#' + i)).prop("data", "false");
            $($('#' + i)).css({ "background-color": "#dc3545", "color": "white" });
            modules.pop($($('#' + i)).attr("data-value"));
        }
    })

    $('#btn_start').on('click', function (event) {
        var sim = $("#sims").children("option:selected").val();
        var updateInterval = $("#updateInterval").val();
        if (sim != $("#sims").children("option:disabled").val()) {
            $('.loading').css({ "visibility": "visible" });
            $('#btn_start').toggleClass('disabled', true);
            $.ajax({
                url: "/processVis",
                type: "POST",
                data: JSON.stringify({ name: sim, modules: modules, updateInterval: updateInterval}),
                contentType: "application/json; charset=utf-8",
                success: function (dat) {
                    window.location.href = "/vis";
                }
            });


        } else {
            console.log("Select first required information");
        }
    })
})