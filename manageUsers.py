""" Manage users.
"""

import sqlite3
import hashlib
from getpass import getpass

conn = sqlite3.connect("info.db")
conn.execute("CREATE TABLE IF NOT EXISTS users(id text,pass text)")
conn.commit()

menu = "1- Add new user.\n2- Remove a user.\n3- Show all users.\n0- Exit."
response = ""

while(response != "0"):
    print(menu)
    response = input("Please select an option: ")
    if response == "1":
        email = input("Email/Username: ")
        password = getpass("Password: ")
        email = hashlib.sha512(bytes(email, encoding='utf-8')).hexdigest()
        password = hashlib.sha512(
            bytes(password, encoding='utf-8')).hexdigest()

        conn.execute("INSERT INTO users VALUES(?,?)", [email, password])
        conn.commit()

        print("Added successfully !")

    elif response == "2":
        email = input("Email/Username: ")

        conn.execute("DELETE FROM users WHERE id=?", (hashlib.sha512(
            bytes(email, encoding='utf-8')).hexdigest(),))
        conn.commit()

    elif response == "3":
        cur = conn.cursor()
        cur.execute("SELECT id FROM users")

        rows = cur.fetchall()

        print("Users id:\n")
        for row in rows:
            print(row, "\n")

conn.close()
