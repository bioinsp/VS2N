"""
Main VS2N class

"""

import _thread
import hashlib
import importlib
import logging
import os
import sqlite3
import time
import traceback
import webbrowser

import dash
import dash_bootstrap_components as dbc
import flask
import flask_login
from flask import Flask, render_template, request
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.serving import run_simple

from src.Global_Var import Global_Var
from src.Modules.General.layout import layout

class User(flask_login.UserMixin):
            pass

class VS2N():
    
    def __init__(self):
        # Print only errors in console ---------------------------------------
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.ERROR)
        # --------------------------------------------------------------------
        self.modulesNbr = 0
        self.mongo_exists = False
        self.app_vis = None
        self.users = dict()
        self.g = Global_Var()
        self.url = self.g.config.VS2N_HOST
        self.port = self.g.config.VS2N_PORT
        
        # Test port availability ---------------------------------------------
        if(not self.g.testPort(self.port)):
            self.port += 1
            print("Loading another instance of VS2N")
        # --------------------------------------------------------------------

        # Create flask app instance ------------------------------------------
        self.flask_app = Flask(__name__, template_folder=os.path.curdir +
                        "/src/templates", static_folder=os.path.curdir + "/src/static")
        self.flask_app.secret_key = self.g.config.SECRET_KEY

        # Flask user preperation ---------------------------------------------

        self.flask_app.config['LOGIN_DISABLED'] = False

        try:
            conn = sqlite3.connect("info.db")
            self.users = dict([u for u in conn.execute("SELECT * FROM users")])
            conn.close()
        except Exception as e:
            # If users database is not generated, disable login
            self.flask_app.config['LOGIN_DISABLED'] = True

        self.login_manager = flask_login.LoginManager()
        self.login_manager.init_app(self.flask_app)
        self.login_manager.session_protection = "strong"

        # BDD connection -----------------------------------------------------
        if __name__ == '__main__':
            self.mongo_exists = self.g.mongoConnect()
        # --------------------------------------------------------------------

        # Create dash app instance -------------------------------------------
        if __name__ == '__main__':
            self.app_vis = dash.Dash(
                __name__,
                requests_pathname_prefix='/vis/',
                external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.FONT_AWESOME],
                suppress_callback_exceptions=True,
                title="VS2N")
            self.app_vis.enable_dev_tools(debug=self.g.config.DEBUG)

        # Login manager ------------------------------------------------------

        @self.login_manager.unauthorized_handler
        def unauthorized_handler():
            """ Redirect any unauthorized access to login screen

            Returns:
                redirection to login
            """
            return flask.redirect(flask.url_for('login'))


        @self.flask_app.errorhandler(404)
        def page_not_found(e):
            """ Handle the 404 error

            Returns:
                redirection to login
            """
            return flask.redirect(flask.url_for('login'))


        @self.login_manager.user_loader
        def user_loader(email):
            """ Load user from email address

            Args:
                email (String): user email

            Returns:
                user instance
            """
            try:
                self.users[email]
            except Exception as e:
                print("exception : user_loader")
                return None

            user = User()
            user.id = email
            return user


        @self.login_manager.request_loader
        def request_loader(request):
            """ Process request for authentication

            Args:
                request

            Returns:
                user instance
            """
            try:
                email = request.form.get('email')
                email = email if email == None else hashlib.sha512(
                    bytes(email, encoding='utf-8')).hexdigest()
                self.users[email]
            except Exception as e:
                return
            user = User()
            user.id = email

            user.is_authenticated = hashlib.sha512(
                bytes(request.form['password'], encoding='utf-8')).hexdigest() == self.users[email]
            return user

        # --------------------------------------------------------------------

        # VS2N Rooting -------------------------------------------------------


        @self.flask_app.route('/login', methods=['GET', 'POST'])
        def login():
            """ Handle requests to '/login' page

            Returns:
                redirection to login if not connected otherwise to home screen
            """
            if self.flask_app.config['LOGIN_DISABLED']:
                return flask.redirect(flask.url_for('home'))
            else:
                if flask.request.method == 'GET':
                    return render_template('login.html')

                email = hashlib.sha512(
                    bytes(flask.request.form['email'], encoding='utf-8')).hexdigest()
                try:
                    if self.users[email] == hashlib.sha512(bytes(flask.request.form['password'], encoding='utf-8')).hexdigest():
                        user = User()
                        user.id = email
                        flask_login.login_user(user)
                        return flask.redirect(flask.url_for('home'))
                except Exception as e:
                    print("Authentication failed !")
                    pass

                return flask.redirect(flask.url_for('login'))


        @self.flask_app.route('/logout')
        def logout():
            """ Logout from VS2N.

            Returns:
                redirection to login screen.
            """
            if self.flask_app.config['LOGIN_DISABLED']:
                return flask.redirect(flask.url_for('home'))
            else:
                flask_login.logout_user()
                return flask.redirect(flask.url_for('login'))


        @self.flask_app.route('/')
        def main():
            """ Handle incoming requests to VS2N and check authentication status

            Returns:
                redirection to home if user is authenticated otherwise to login screen
            """
            if self.flask_app.config['LOGIN_DISABLED']:
                return flask.redirect(flask.url_for('home'))
            else:
                if not flask_login.current_user.is_authenticated:
                    return render_template('login.html')
                else:
                    return flask.redirect(flask.url_for('home'))


        @self.flask_app.route('/home')
        @flask_login.login_required
        def home():
            """ Get existing modules and check MongoDB availability

            Returns:
                main screen if everything is ok, otherwise show exception
            """

            # if MongoDB exist
            if self.mongo_exists:
                m = os.listdir(os.path.curdir + "/src/Modules")
                simulations = []
                self.g.db = self.g.client.list_database_names()

                # Load existing simulations
                for bdd in self.g.db:
                    if bdd != "local" and bdd != "admin" and bdd != "config":
                        simulations.append(bdd)
                i = 1

                # get modules names
                modules = []
                for module in m:
                    if ".pyc" not in module and "__" not in module and "General" not in module:
                        modules.append(module.rsplit(".", 1)[0])
                        i = i + 1
                sparkVersion = self.g.createSparkSession()
                # return main screen with list of modules and existing simulations
                return render_template('index.html', modules=modules, Simulations=simulations, mongodb=self.g.client.server_info()["version"], vs2n=self.g.config.VS2N_VERSION, spark=sparkVersion)
            else:
                self.mongo_exists = self.g.mongoConnect()
                return render_template('exception.html')
        
        @self.flask_app.route('/processVis', methods=['POST'])
        @flask_login.login_required
        def processing():
            """ Start processing and load modules into dashboard
            """
            self.g.name = request.get_json()["name"]
            self.g.modules = request.get_json()["modules"]
            self.g.updateInterval = float(request.get_json()["updateInterval"])

            self.g.modules.append("General")
            self.g.modulesNbr = len(self.g.modules)

            self.LoadingModules()

            while (self.g.modulesNbr != 0):
                time.sleep(1)
                continue

            # Loading Modules to Dashboard
            layout().load(self.app_vis, self.g)
            return "done"
        
    def Module(self, n, g):
        """ Helper function to execute each module sparks pre processing (if exist)

        Args:
            n (String): module name
            g (Global_Var): reference to access global variables
        """
        importlib.import_module("src.Modules." + n + ".spark").init(g)


    def LoadingModules(self):
        """ Run spark operation for the selected modules
        """
        try:
            # Spark & MongoDB tasks launch
            self.g.CreateIndexes(self.g.name)
            for module in self.g.modules:
                _thread.start_new_thread(self.Module, (module, self.g,))

        except Exception as e:
            print("LoadingModules:" + str(e))


    # Start VS2N ---------------------------------------------------------


    def start_VS2N(self):
        """ Start the virtual server on the web browser

        Returns:
            Dash server
        """
        print("Starting VS2N")
        self.app_vis.serve_reload_hash

        webbrowser.open("http://"+self.url+":"+str(self.port))
        return self.app_vis.server

    # Start server -------------------------------------------------------


if __name__ == '__main__':
    try:
        vs2n = VS2N()
        application = DispatcherMiddleware(vs2n.flask_app, {'/vis': vs2n.start_VS2N()})
        run_simple(vs2n.url, vs2n.port, application, use_debugger=vs2n.g.config.DEBUG)
    except Exception as e:
        traceback.print_exc()
