import pytest
from VS2N import flask_app

@pytest.fixture
def flask_app():
    yield flask_app

#@pytest.fixture
#def dashApp():
#    # Init dash app
#    app_vis = dash.Dash(
#        __name__,
#        requests_pathname_prefix='/vis/',
#        external_stylesheets=[VS2N.dbc.themes.BOOTSTRAP, VS2N.dbc.icons.FONT_AWESOME],
#        suppress_callback_exceptions=True,
#        title="VS2N")
#    
#    app_vis.enable_dev_tools(debug=VS2N.g.config.DEBUG)
#
#    # Loading modules
#
#    request = {'name': 'csnn-23-3-2023-16:32:48', 'modules': ['Neuron', 'Synapse'], 'updateInterval': '1'}
#    
#    yield app_vis