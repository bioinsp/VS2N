""" Configuration file .
"""


class config():

    VS2N_HOST = "0.0.0.0"
    VS2N_PORT = 8050
    VS2N_VERSION= "0.36"
    DEBUG = False
    DATABASE_URL = "mongodb://127.0.0.1:27017/"
    USERNAME = "" #USERNAME HERE
    PASSWORD = "" #PASSWORD HERE
    MONGODBURL = "mongodb://"+USERNAME+":"+PASSWORD+"@127.0.0.1:27017/"
    SECRET_KEY = 'SECRETKEY' # Secret key to be used later on to sign session cookies (if needed)
