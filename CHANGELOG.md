# Changes Log

## v0.36	

### Core
- Multilayer support added
- Spark & MongoDB version updated

## v0.35	

### Core
- Graphs improved & fixed typos.
- UI updated.

## v0.34	

### Core
- Added authentication.
- Added support for Nengo simulator.

## v0.33	

Added another module for synapses activity with other improvements and
bugs fix.

### Core
- Add support for MongoDB access using credentials.
- Tested other similar datasets to MNIST (FMNIST)
- Fixed performance issue loading modules
- Fixed labels counting issue
- UI updated
  
### Visualization
Modules:
- Showing better information on hover 
- Show text time interval on hover instead of x Axis to save space

![SynapseModule](img/SynapseModule.png)
- Synapse Module:
  - Get data from MongoDB
  - Ability to select neurons
  - Dynamically add graphs
  - Selection of HeatMap dimensions
  - Show HeatMap of selected neurons
  - Show synapse mean frequency activity graph of selected neurons
  - Show [global HeatMap](img/SynapseModule2.png) of the output layer

## v0.32

### Core
- project structure organisation
- fix duplicates callbacks issue
- fix clean data when moving around using unified callback
- fix not taking all data in slider
- Fix class save format in database
- reduce class files
- module template added
- remove global and replace with class and self

### Visualization
![NeuronModule](img/NeuronModule.png)
- Neuron Module:
  - get data from MongoDB
  - ability to select neurons
  - dynamically add graphs
  - Spike freq of output layer (3D)
  - Spike number by Class
  - add spark operations
  - reset graphs if slider moves away
  - Add on/off button
  - show potential change of selected neurons
  - show spikes activity of selected neurons
  - show accuracy of selected neurons

## v0.3

Third version with a focus on performance while getting data for
visualization and code cleaning

### Core
- Code cleaned.
- Callback Interval now doesn't run every time even when no activity is
  done using the tool.
- One Callback to organize all the graphs and visualization.
- proposed a name for the Tool **VS2N** (**V**isualization for **S**piking **N**eural **N**etworks)
- MongoDB Index creation
- Fix back button issue
- Check existence of labels information before adding the visualization
- Better organization of Table information:
  - Added network accuracy
- Restart MongoDB when tasks are executed first time to free some memory

### Visualization
- Added Max horizontal bar for general graph
- Added network loss graph 
- Added global input value
- Added Disable/Enable button for all graphs

## v0.2

Second version with a focus on performance optimization and better way
to create and present visualizations

### Core
- Added Template file for new Modules creation
- added post processing using MongoDB for general visualizations.
- Modified MongoDB schema for more data collection:
  - Network related information.
  - Classification accuracy.
  - Class labels.

### Visualization
- Replace D3.js with Dash for visualization.
- Added general visualization of:
  - Spikes.
  - Potential changes.
  - Synapses updates.
  - Labels introduced. 

## v0.1

First version with a focus on backend and global organization of the
tool.

### Core
- Python backend single setup (remove JS)
- Configuration of Spark and MongoDB dependencies
- Extraction of MongoDB data

### Visualization
- Use of D3.js library for graphs
- Simple graph tested
